import { createLocalVue, shallowMount, mount } from '@vue/test-utils'
import HomeView from '@/components/HomeView.vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vue from 'vue'

describe('HomeView.vue', () => {
  const router = new VueRouter()
  const localVue = createLocalVue()
  localVue.use(VueRouter)
  Vue.use(Vuetify)
  // Don't use localVue.use(Vuetify) because of the bug. Refer to https://github.com/vuetifyjs/vuetify/issues/4964.
  // localVue.use(Vuetify);

  let wrapper = null
  const mockQueryTotalBooks = jest.fn()
  const mockQueryTotalAuthors = jest.fn()
  const mockQueryTotalDonors = jest.fn()

  it('methods queryTotalBooks, queryTotalAuthors and queryTotalDonors should be called once when HomeView created', () => {
    wrapper = shallowMount(HomeView, {
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalAuthors: mockQueryTotalAuthors,
        queryTotalDonors: mockQueryTotalDonors
      }
    })

    expect(mockQueryTotalBooks).toHaveBeenCalled()
    expect(mockQueryTotalAuthors).toHaveBeenCalled()
    expect(mockQueryTotalDonors).toHaveBeenCalled()
  })

  it('method queryTotalBooks sets the correct value to totalBooks', () => {
    const testTotalBooks = 299
    const $http = {
      get: () => {
        return $http
      }, 
      then: (callback) => {
        let response = {
          body: {
            total: testTotalBooks
          }
        }
        callback(response)
        return $http
      },
      catch: () => {}
    }

    wrapper = shallowMount(HomeView, {
      methods: {
        queryTotalAuthors: mockQueryTotalAuthors,
        queryTotalDonors: mockQueryTotalDonors
      },
      mocks: {
        $http
      }
    })

    expect(wrapper.vm.totalBooks).toBe(testTotalBooks)
  })

  it('When http get fails, method queryTotalBooks should throw an error and set undefined to totalBooks', () => {
    const $http = {
      get: () => {
        return $http
      }, 
      then: () => {
        return $http
      },
      catch: (callback) => {
        let errorMsg = {}
        callback(errorMsg)
      }
    }

    wrapper = shallowMount(HomeView, {
      methods: {
        queryTotalAuthors: mockQueryTotalAuthors,
        queryTotalDonors: mockQueryTotalDonors
      },
      mocks: {
        $http
      }
    })

    expect(wrapper.vm.totalBooks).toBe(undefined)
  })

  it('method queryTotalAuthors sets the correct value to totalAuthors', () => {
    const testTotalAuthors = 4351
    const $http = {
      get: () => {
        return $http
      }, 
      then: (callback) => {
        let response = {
          body: {
            total: testTotalAuthors
          }
        }
        callback(response)
        return $http
      },
      catch: () => {}
    }

    wrapper = shallowMount(HomeView, {
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalDonors: mockQueryTotalDonors
      },
      mocks: {
        $http
      }
    })

    expect(wrapper.vm.totalAuthors).toBe(testTotalAuthors)
  })

  it('When http get fails, method queryTotalAuthors should throw an error and set undefined to totalAuthors', () => {
    const $http = {
      get: () => {
        return $http
      }, 
      then: () => {
        return $http
      },
      catch: (callback) => {
        let errorMsg = {}
        callback(errorMsg)
      }
    }

    wrapper = shallowMount(HomeView, {
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalDonors: mockQueryTotalDonors
      },
      mocks: {
        $http
      }
    })

    expect(wrapper.vm.totalAuthors).toBe(undefined)
  })

  it('method queryTotalDonors sets the correct value to totalDonors', () => {
    const testTotalDonors = 9527
    const $http = {
      get: () => {
        return $http
      }, 
      then: (callback) => {
        let response = {
          body: {
            total: testTotalDonors
          }
        }
        callback(response)
        return $http
      },
      catch: () => {}
    }

    wrapper = shallowMount(HomeView, {
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalAuthors: mockQueryTotalAuthors
      },
      mocks: {
        $http
      }
    })

    expect(wrapper.vm.totalDonors).toBe(testTotalDonors)
  })

  it('When http get fails, method queryTotalDonors should throw an error and set undefined to totalDonors', () => {
    const $http = {
      get: () => {
        return $http
      }, 
      then: () => {
        return $http
      },
      catch: (callback) => {
        let errorMsg = {}
        callback(errorMsg)
      }
    }

    wrapper = shallowMount(HomeView, {
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalAuthors: mockQueryTotalAuthors
      },
      mocks: {
        $http
      }
    })

    expect(wrapper.vm.totalDonors).toBe(undefined)
  })

  it('click bookList button should go to /books', () => {
    // Reference: https://medium.com/js-dojo/unit-testing-vue-router-1d091241312
    wrapper = mount(HomeView, {
      localVue,
      router,
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalAuthors: mockQueryTotalAuthors,
        queryTotalDonors: mockQueryTotalDonors
      }
    })

    wrapper.find("#btn_to_books").trigger("click")
    expect(wrapper.vm.$route.path).toBe('/books')
  })

  it('click authorList button should go to /authors', () => {
    wrapper = mount(HomeView, {
      localVue,
      router,
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalAuthors: mockQueryTotalAuthors,
        queryTotalDonors: mockQueryTotalDonors
      }
    })

    wrapper.find("#btn_to_authors").trigger("click")
    expect(wrapper.vm.$route.path).toBe('/authors')

  })

  it('click donorList button should go to /donors', () => {
    wrapper = mount(HomeView, {
      localVue,
      router,
      methods: {
        queryTotalBooks: mockQueryTotalBooks,
        queryTotalAuthors: mockQueryTotalAuthors,
        queryTotalDonors: mockQueryTotalDonors
      }
    })

    wrapper.find("#btn_to_donors").trigger("click")
    expect(wrapper.vm.$route.path).toBe('/donors')

  })
})
