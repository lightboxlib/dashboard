import Vue from 'vue'
import './plugins/vuetify'
import './plugins/vue-resource'
import './plugins/vee-validate'
import router from './plugins/vue-router'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
