import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '../components/HomeView.vue'
import BooksListView from '../components/books/BooksListView.vue'
import DetailedBookInfoView from '../components/books/DetailedBookInfoView.vue'
import UpdateBookView from '../components/books/UpdateBookView'
import CreateBookView from '../components/books/CreateBookView'
import AuthorListView from '../components/authors/AuthorListView'
import DetailedAuthorInfoView from '../components/authors/DetailedAuthorInfoView.vue'
import UpdateAuthorView from '../components/authors/UpdateAuthorView'
import CreateAuthorView from '../components/authors/CreateAuthorView'
import DonorListView from '../components/donors/DonorListView'
import DetailedDonorInfoView from '../components/donors/DetailedDonorInfoView'
import CreateDonorView from '../components/donors/CreateDonorView'
import UpdateDonorView from '../components/donors/UpdateDonorView'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'HomeView',
      component: HomeView
    },
    {
      path: '/books',
      name: 'BooksListView',
      component: BooksListView
    },
    {
      path: '/books/:id',
      name: 'DetailedBookInfoView',
      component: DetailedBookInfoView
    },
    {
      path: '/books/edit/:id',
      name: 'UpdateBookView',
      component: UpdateBookView
    },
    {
      path: '/books/create',
      name: 'CreateBookView',
      component: CreateBookView
    },
    {
      path: '/authors',
      name: 'AuthorListView',
      component: AuthorListView
    },
    {
      path: '/authors/:id',
      name: 'DetailedAuthorInfoView',
      component: DetailedAuthorInfoView
    },
    {
      path: '/authors/edit/:author',
      name: 'UpdateAuthorView',
      component: UpdateAuthorView
    },
    {
      path: '/authors/create',
      name: 'CreateAuthorView',
      component: CreateAuthorView
    },
    {
      path: '/donors',
      name: 'DonorListView',
      component: DonorListView
    },
    {
      path: '/donors/:id',
      name: 'DetailedDonorInfoView',
      component: DetailedDonorInfoView
    },
    {
      path: '/donors/create',
      name: 'CreateDonorView',
      component: CreateDonorView
    },
    {
      path: '/donors/edit/:id',
      name: 'UpdateDonorView',
      component: UpdateDonorView
    }
  ]
})
